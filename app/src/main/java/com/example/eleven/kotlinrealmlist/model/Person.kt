package com.example.eleven.kotlinrealmlist.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by ELEVEN on 7/28/2017.
 */
open class Person() : RealmObject()  {

    @PrimaryKey var personID: Long = 0
    var personName: String? = ""
    var personStatus: String? = ""
    var personLocation: String? = ""

    //one is to many relation
    var birds: RealmList<Animal> = RealmList()

    constructor(name: String, status: String, location: String): this() {
        personName = name
        personStatus = status
        personLocation = location
    }

}