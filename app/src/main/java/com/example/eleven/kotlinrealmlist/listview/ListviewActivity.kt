package com.example.eleven.kotlinrealmlist.listview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.example.eleven.kotlinrealmlist.R
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_listview.*
import kotlinx.android.synthetic.main.activity_recyclerview.*
import kotlin.properties.Delegates

class ListviewActivity : AppCompatActivity() {

    private var realm: Realm by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listview)
        Realm.init(this)
        realm = Realm.getDefaultInstance()
    }

    fun deleteDataFromRealm(){
        realm.executeTransaction { realm.deleteAll() }
    }

    override fun onDestroy() {
        super.onDestroy()
        listview_realm.adapter = null
        realm.close()
    }
}
